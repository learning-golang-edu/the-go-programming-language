package main

import (
	"greeting"
	"fmt"
)

func RenameToFroq(r greeting.Renamable) {
	r.Rename("Frog")
}

func main() {
	salutations := greeting.Salutations{
		{"Edu", "Hello"},
		{"Arno", "Hi"},
		{"Janna", "What's up?"},
	}

	salutations[0].Rename("Lauri")

	RenameToFroq(&salutations[2])

	fmt.Fprintf(&salutations[1], "The cound is %d", 10)

	salutations.Greet(greeting.CreatePrintFunction("!"), true)
}

package greeting

import "fmt"

type Salutation struct {
	Name     string
	Greeting string
}

type Renamable interface {
	Rename(newName string)
}

func (salutation *Salutation) Rename(newName string) {
	salutation.Name = newName
}

func (salutation *Salutation) Write(p []byte) (n int, err error) {
	s := string(p)
	salutation.Rename(s)
	n = len(s)
	err = nil
	return
}

type Salutations []Salutation

func (salutations Salutations) Greet(do Printer, isFormal bool) {
	for _, s := range salutations {
		message, alternate := CreateMessage(s.Name, s.Greeting)
		if prefix := GetPrefix(s.Name); isFormal {
			do(prefix + message)
		} else {
			do(alternate)
		}
	}
}

func GetPrefix(name string) (prefix string) {
	prefixMap := map[string]string{
		"Edu":   "Mr ",
		"Arno":  "Dr ",
		"Alice": "Dr ",
		"Janna": "Mrs ",
	}
	prefixMap["Arno"] = "Jr "

	delete(prefixMap, "Janna")

	if value, exists := prefixMap[name]; exists {
		return value
	}
	return "Dude "
}

func CreateMessage(name, greeting string) (message string, alternate string) {
	message = greeting + ", " + name
	alternate = "Hey, " + name
	return
}

type Printer func(string) ()

func CreatePrintFunction(custom string) Printer {
	return func(s string) {
		fmt.Println(s + custom)
	}
}

package greeting

import "fmt"

type Salutation struct {
	Name     string
	Greeting string
}

func GetPrefix(name string) (prefix string) {
	switch name {
	case "Edu":
		prefix = "Mr "
	case "Arno", "Alice":
		prefix = "Dr "
	case "Janna":
		prefix = "Mrs "
	default:
		prefix = "Dude "
	}
	return
}

func GetPrefixWithNothingSwitch(name string) (prefix string) {
	switch {
	case name == "Edu":
		prefix = "Mr "
	case name == "Arno", name == "Alice", len(name) == 10:
		prefix = "Dr "
	case name == "Janna":
		prefix = "Mrs "
	default:
		prefix = "Dude "
	}
	return
}

func TypeSwitchText(x interface{}) {
	switch x.(type) {
	case int:
		fmt.Println("int")
	case string:
		fmt.Println("string")
	case Salutation:
		fmt.Println("salutation")
	default:
		fmt.Println("unknown")
	}
}

func CreateMessage(name, greeting string) (message string, alternate string) {
	message = greeting + ", " + name
	alternate = "Hey, " + name
	return
}

type Printer func(string) ()

func Greet(salutation Salutation, do Printer, isFormal bool) {
	message, alternate := CreateMessage(salutation.Name, salutation.Greeting)
	if prefix := GetPrefix(salutation.Name); isFormal {
		do(prefix + message)
	} else {
		do(alternate)
	}
}

func CreatePrintFunction(custom string) Printer {
	return func(s string) {
		fmt.Println(s + custom)
	}
}

package main

import "./greeting"

func main() {
	s := greeting.Salutation{"Edu", "Hello"}
	greeting.Greet(s, greeting.CreatePrintFunction("!"), true)
	greeting.Greet(s, greeting.CreatePrintFunction("!"), false)
	s1 := greeting.Salutation{"Arno", "Hi"}
	greeting.Greet(s1, greeting.CreatePrintFunction("!"), true)
	s2 := greeting.Salutation{"Janna", "Hello"}
	greeting.Greet(s2, greeting.CreatePrintFunction("!"), true)
	s3 := greeting.Salutation{"Lauri", "Hi"}
	greeting.Greet(s3, greeting.CreatePrintFunction("!"), true)
	s4 := greeting.Salutation{"Alice", "Hello"}
	greeting.Greet(s4, greeting.CreatePrintFunction("!"), true)

	greeting.TypeSwitchText("hello")
	greeting.TypeSwitchText(123)
	greeting.TypeSwitchText(s)
	greeting.TypeSwitchText(1.234)
}

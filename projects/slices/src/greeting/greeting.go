package greeting

import "fmt"

type Salutation struct {
	Name     string
	Greeting string
}

func GetPrefix(name string) (prefix string) {
	prefixMap := map[string]string{
		"Edu":   "Mr ",
		"Arno":  "Dr ",
		"Alice": "Dr ",
		"Janna": "Mrs ",
	}
	prefixMap["Arno"] = "Jr "

	delete(prefixMap, "Janna")

	if value, exists := prefixMap[name]; exists {
		return value
	}
	return "Dude "
}

func CreateMessage(name, greeting string) (message string, alternate string) {
	message = greeting + ", " + name
	alternate = "Hey, " + name
	return
}

type Printer func(string) ()

func Greet(salutations []Salutation, do Printer, isFormal bool) {
	for _, s := range salutations {
		message, alternate := CreateMessage(s.Name, s.Greeting)
		if prefix := GetPrefix(s.Name); isFormal {
			do(prefix + message)
		} else {
			do(alternate)
		}
	}
}

func CreatePrintFunction(custom string) Printer {
	return func(s string) {
		fmt.Println(s + custom)
	}
}

package main

import (
	"greeting"
	"fmt"
)

func main() {
	s := make([]int, 3)
	s[0] = 1
	s[1] = 10
	s[2] = 500
	// s[3] = 56 // this causes index out of range error

	s2 := []int{1, 10, 500, 56}
	fmt.Println(s2)
	fmt.Println(s2[1:2])
	fmt.Println(s2[:2])
	fmt.Println(s2[2:])

	slice := []greeting.Salutation{
		{"Edu", "Hello"},
		{ "Arno", "Hi"},
		{ "Janna", "What's up?"},
	}

	slice = append(slice, greeting.Salutation{"Lauri", "Hi"})
	greeting.Greet(slice, greeting.CreatePrintFunction("!"), true)
	fmt.Println("=========")

	slice = append(slice[:1], slice[2:]...) // deleting item at index 1
	greeting.Greet(slice, greeting.CreatePrintFunction("!"), true)
}

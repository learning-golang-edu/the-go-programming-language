package main

import "greeting"

func main() {
	slice := []greeting.Salutation{
		{"Edu", "Hello"},
		{ "Arno", "Hi"},
		{ "Janna", "What's up?"},
	}
	greeting.Greet(slice, greeting.CreatePrintFunction("!"), true)
}

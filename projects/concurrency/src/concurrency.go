package main

import (
	"greeting"
	"fmt"
)

func main() {
	salutations := greeting.Salutations{
		{"Edu", "Hello"},
		{"Arno", "Hi"},
		{"Janna", "What's up?"},
	}

	done := make(chan bool, 2)

	go func() {
		salutations.Greet(greeting.CreatePrintFunction("<C>"), true)
		done <- true
		done <- true
		fmt.Println("Done!")
	}()
	salutations.Greet(greeting.CreatePrintFunction("!"), true)
	<- done

	// channel with range
	c := make(chan greeting.Salutation)
	go salutations.ChannelGreeter(c)
	for s := range c {
		fmt.Println(s.Name)
	}

	// select example
	c1 := make(chan greeting.Salutation)
	c2 := make(chan greeting.Salutation)
	go salutations.ChannelGreeter(c1)
	go salutations.ChannelGreeter(c2)

	for {
		select {
		case s, ok := <- c1:
			if ok {
				fmt.Println(s, ":1")
			} else {
				return
			}
		case s, ok := <- c2:
			if ok {
				fmt.Println(s, ":2")
			} else {
				return
			}
		default:
			fmt.Println("Waiting...")
		}
	}
}

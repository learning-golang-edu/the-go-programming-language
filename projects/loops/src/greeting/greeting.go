package greeting

import "fmt"

type Salutation struct {
	Name     string
	Greeting string
}

func GetPrefix(name string) (prefix string) {
	switch name {
	case "Edu":
		prefix = "Mr "
	case "Arno", "Alice":
		prefix = "Dr "
	case "Janna":
		prefix = "Mrs "
	default:
		prefix = "Dude "
	}
	return
}

func CreateMessage(name, greeting string) (message string, alternate string) {
	message = greeting + ", " + name
	alternate = "Hey, " + name
	return
}

type Printer func(string) ()

func Greet(salutation Salutation, do Printer, isFormal bool, times int) {
	message, alternate := CreateMessage(salutation.Name, salutation.Greeting)

	for i := 0; i < times; i++ {
		if prefix := GetPrefix(salutation.Name); isFormal {
			do(prefix + message)
		} else {
			do(alternate)
		}
	}

	i := 0
	for i < times {
		fmt.Println(i)
		i++
	}

	i = 0
	for {
		if i >= times {
			break
		}
		if i % 2 == 0 {
			i++
			continue
		}
		fmt.Println("infinite", i)
		i++
	}
}

func GreetSlice(salutations []Salutation, do Printer, isFormal bool) {
	for _, s := range salutations {
		message, alternate := CreateMessage(s.Name, s.Greeting)
		if prefix := GetPrefix(s.Name); isFormal {
			do(prefix + message)
		} else {
			do(alternate)
		}
	}
}

func CreatePrintFunction(custom string) Printer {
	return func(s string) {
		fmt.Println(s + custom)
	}
}

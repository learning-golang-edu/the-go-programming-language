package main

import "./greeting"

func main() {
	s := greeting.Salutation{"Edu", "Hello"}
	greeting.Greet(s, greeting.CreatePrintFunction("!"), true, 5)

	slice := []greeting.Salutation{
		{"Arno", "Hello"},
		{ "Janna", "Hi"},
		{ "Lauri", "What's up?"},
	}
	greeting.GreetSlice(slice, greeting.CreatePrintFunction("!"), true)
}

package main

import "fmt"

type Salutation struct {
	name     string
	greeting string
}

func CreateMessage(name, greeting string) string {
	return greeting + " " + name
}

func CreateMessageWithAlternate(name, greeting string) (string, string) {
	return greeting + " " + name, "Hey! " + name
}

func CreateMessageWithAlternate2(name, greeting string) (message string, alternate string) {
	message = greeting + ", " + name + "!"
	alternate = "Hey, " + name + "!"
	return
}

func CreateMessageVariadic(name string, greeting ...string) (message string, alternate string) {
	message = greeting[0] + ", " + name + "!"
	alternate = greeting[1] + " " + name + "!"
	return
}

func Greet(salutation Salutation) {
	fmt.Println(CreateMessage(salutation.name, salutation.greeting))
	_, alternate := CreateMessageWithAlternate(salutation.name, salutation.greeting)
	fmt.Println(alternate)
	message, alternate1 := CreateMessageWithAlternate2(salutation.name, salutation.greeting)
	fmt.Println(message)
	fmt.Println(alternate1)
	message2, alternate2 := CreateMessageVariadic(salutation.name, salutation.greeting, "yo")
	fmt.Println(message2)
	fmt.Println(alternate2)
}

type Printer func(string) ()

func GreetWithFunc(salutation Salutation, do Printer) {
	message, alternate := CreateMessageWithAlternate2(salutation.name, salutation.greeting)
	do(message)
	do(alternate)
}

func Print(s string) {
	fmt.Print(s)
}

func PrintLine(s string) {
	fmt.Println(s)
}

func CreatePrintFunction(custom string) Printer {
	return func(s string) {
		fmt.Println(s + custom)
	}
}

func main() {
	s := Salutation{"Edu", "Hello"}
	Greet(s)
	GreetWithFunc(s, Print)
	fmt.Println()
	GreetWithFunc(s, PrintLine)
	GreetWithFunc(s, CreatePrintFunction("!!"))
}
